#include<stdio.h> 
int fib(int);

int main(void)
{    
    int n, i;

    printf("Enter n number: ");
    scanf("%d", &n);       

    for( i = 0; i <= n; i++)
    {
        printf("%d \n", fib(i));
    }

    return 0; 
}

int fib(int num)
{    

    if(num == 0 || num == 1)
    {
        return num;
    }

    else
    {
    
        return fib(num-1) + fib(num-2);
    }

}
